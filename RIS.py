import requests
import re
import bs4
def GRIS(imgurl, session):
    url='https://www.google.com/searchbyimage?&image_url=%s' % imgurl
    r=session.get(url)
    html=r.text
    for i in re.findall(r'<script(.*?)script', html):
        html=html.replace('<script%sscript>' % i, '')
        html=html.replace('<script%sscript/>' % i, '')
    soup=bs4.BeautifulSoup(r.text)
    keywords=[]
    for i in soup.find_all('a'):
        try:
            if '/search?q' in i.attrs['href']:
                if 'cache' not in i.attrs['href']:
                    if 'related' not in i.attrs['href']:
                        if i.getText():
                            keywords.append(i.getText())
        except Exception:
            pass
    keywords=list(set(keywords))
    return keywords
