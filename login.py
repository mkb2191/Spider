import bs4
#needs a requests session object
def GoogleLogin(email, passd, session):
    url='https://accounts.google.com/ServiceLogin'
    r=session.get(url)
    session.headers.update({'Referer':url})
    values=bs4.BeautifulSoup(r.text).find('form').find_all('input')
    data={}
    for u in values:
        if u.has_attr('value'):
            data[u['name']]=u['value']
    data.update({'Passd':passd,
                 'Email':email})
    session.post('https://accounts.google.com/ServiceLoginAuth', data=data)
    return session
